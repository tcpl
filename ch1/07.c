/*
 * Exercise 1-7:
 * Write a program to print the value of EOF.
 */

#include <stdlib.h>
#include <stdio.h>

int
main(void)
{
	// EOF is actually a macro to an integer, as defined in stdio.h.
	printf("The value of EOF is %i.\n", EOF);

	return EXIT_SUCCESS;
}
