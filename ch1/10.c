/*
 * Example 1-10:
 * Write a program to copy its input to its output, replacing each tab by \t, each backspace by \b,
 * and each backslash by \\.  This makes tabs and backspaces visible in an unambiguous way.
 */

// NOTE: On most modern systems, backspaces will not show up, because of buffered output.

#include <stdlib.h>
#include <stdio.h>

/*
 * Print this when we need to show a special character.
 * It itself is escaped, because a backslash is also a special character!
 */
#define ESC_CHAR '\\'

int
main(void)
{
	// Read all input until EOF.
	int c;
	while ((c = getchar()) != EOF)
		// Show special characters by printing ESC_CHAR and then a letter.
		switch (c) {
			case '\t':
				putchar(ESC_CHAR);
				putchar('t');
				break;
			case '\b':
				putchar(ESC_CHAR);
				putchar('b');
				break;
			case ESC_CHAR:
				putchar(ESC_CHAR);
				putchar(ESC_CHAR);
				break;
			default:
				putchar(c);
		}

	return EXIT_SUCCESS;
}
