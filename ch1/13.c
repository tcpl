/*
 * Example 1-13:
 * Write a program to print a histogram of the lengths of words in its input.
 * It is easy to draw the histogram with the bars horizontal;
 * a vertical orientation is more challenging.
 */

/*
 * NOTE: Word separator tokens are isspace() and EOF.
 *
 * TODO: Split giant main() into functions?
 *       Make output graph even more flexible
 *       Eliminate all fixed graph output
 *       Support MAX_CHARS > 100
 *       Prove working (unit tests?)
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>       /* for isspace() */

#define MAX_CHARS 10     /* [< 100] longer words placed in separate column */
#define BAR_CHAR  '*'    /* char used in bars */
#define BAR_SEP   "  "   /* spaces between bars */

int
main(void)
{
	/*
	 * Occurance freqs of n-character words - words[n].
	 * words[MAX_CHARS+1] is for words > MAX_CHARS.
	 * Init all indices to 0.
	 */
	int words[MAX_CHARS+1] = {0};

	/*
	 * Read input: Count char num in each word (n); store as words[n-1].
	 * Record last char as lastchar, only needed to check for newline.
	 */
	int lastchar = EOF;     /* last input char */
	int chars = 0;          /* chars in each word */
	int c;
	while ( (c = getchar()) ) {
		if (!isspace(c) && c != EOF)
			++chars;
		else {
			// End of word/file; bump freq for last word's length.
			if (chars <= MAX_CHARS)
				++words[chars-1];
			else
				++words[MAX_CHARS];    /* long word, last col */

			if (EOF == c)
				break;
			chars = 0;        /* prep for next word */
			lastchar = c;     /* newlines are isspace() */
		}
	}

	// Ensure we're on a new line before printing.
	if (lastchar != '\n') {
		putchar('\n');
	}

	// Longest bar needs first print; find it.
	int longest_bar = 0;
	for (int bar = 1; bar <= MAX_CHARS; ++bar)
		if (words[longest_bar] < words[bar])
			longest_bar = bar;

	/*
	 * Print main graph.
	 */
	for (int y = words[longest_bar]; y > 0; --y) {
		// Y-axis
		printf("%4d  | ", y);

		// Bars
		for (int bar = 0; bar < MAX_CHARS; ++bar) {
			if (words[bar] >= y)
				putchar(BAR_CHAR);
			else
				putchar(' ');
			printf(BAR_SEP);      /* bar separator */
		}
		putchar(BAR_CHAR);
		putchar('\n');       /* avoid extra spaces */
	}

	// Print bottom border.
	printf("      +-");
	for (int bar = 0; bar < MAX_CHARS; ++bar)
		printf("---");
	printf("--\n");              /* avoid extra spaces */

	// Print x-axis.
	printf("       ");
	for (int bar = 0; bar < MAX_CHARS; ++bar)
		printf("%2d ", bar+1);

	// Avoid extra spaces, and use '>' for last bar.
	printf(">%d\n", MAX_CHARS);

	return EXIT_SUCCESS;
}
