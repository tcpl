/*
 * Exercise 1-6:
 * Verify that the expression getchar() != EOF is 0 or 1.
 *
 * This program prompts for input, and then uses the last input character in the evaluation
 * getchar() != EOF.  If the char was EOF, 0 is returned (false).  Otherwise, 1 (true).
 * If your input is buffered, you will need to use the Enter key to send the program the input.
 */

#include <stdlib.h>
#include <stdio.h>

int
main(void)
{
	printf("Press a key. Try sending EOF, for example.\n");
	printf("The expression `getchar() != EOF` evaluates to %i.\n", getchar() != EOF);

	return EXIT_SUCCESS;
}
