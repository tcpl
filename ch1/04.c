/*
 * Exercise 1-4:
 * Write a program to print the corresponding Celsius to Fahrenheit table.
 *
 * Example program to modify given in file 'tempconv.c'
 */

#include <stdlib.h>
#include <stdio.h>

// Convert the numbers in STEP increments between LOWER and UPPER.
#define LOWER 0
#define UPPER 300
#define STEP  20

int
main(void)
{
	float fahr, celsius;

	printf("Celc -> Fahr\n");  // Header

	for (celsius = LOWER; celsius <= UPPER; celsius += STEP) {
		/*
		 * Basic Celcius -> Fahrenheit forumula.
		 * The 'fahr = ...' step is not necessary; the '...' could be used
		 * as an argument to printf() by itself.  But, this is just a learning tool.
		 */
		fahr = (9.0/5.0) * (celsius+32);

		// In x.yf: x is the width to pad with spaces to, and y is the decimal precision
		printf("%3.0f %8.1f\n", celsius, fahr);
	}

	return EXIT_SUCCESS;
}
