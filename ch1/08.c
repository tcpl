/*
 * Exercise 1-8:
 * Write a program to count blanks, tabs, and newlines.
 */

#include <stdlib.h>
#include <stdio.h>

int
main(void)
{
	// Counters for blanks (spaces), tabs, and newlines (respectively).
	int spc = 0, tab = 0, nl = 0;

	/*
	 * Loop through all input until EOF is hit.
	 * Increment the counters when the counters' characters are hit.
	 */
	int c;
	while ((c = getchar()) != EOF)
		switch (c) {
			case ' ':
				++spc; break;
			case '\t':
				++tab; break;
			case '\n':
				++nl; break;
		}

	// Print sums.
	printf("Blanks: %i Tabs: %i Lines: %i\n", spc, tab, nl);

	return EXIT_SUCCESS;
}
