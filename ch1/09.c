/*
 * Exercise 1-9:
 * Write a program to copy its input to its output, replacing each string of one or more blanks by a
 * single blank.
 */

#include <stdlib.h>
#include <stdio.h>

int
main(void)
{
	// Read all input until EOF.
	int c;
	while ((c = getchar()) != EOF) {
		/*
		 * Upon reaching a space, print said space, and then keep looping
		 * until we get back to non-space characters.
		 */
		if (c == ' ') {
			putchar(c);
			while ((c = getchar()) == ' ' && c != EOF)
				;
		}

		if (c == EOF)
			break;

		putchar(c);
	}

	return EXIT_SUCCESS;
}
