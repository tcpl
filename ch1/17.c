/*
 * Example 1-17:
 * Write a program to print all input lines that are longer than 80 characters.
 */

#include <stdlib.h>
#include <stdio.h>

#define MIN_LENGTH 81  // Print lines longer than this
//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa80chartest

int main(void)
{
	char line[MIN_LENGTH];
	int len = 0;

	int c;
	// Process the entire input
	while ((c = getchar()) != EOF) {
		if (c != '\n') {
			if (len <= MIN_LENGTH)  // Save each character until we reach the print limit
				line[len++] = c;
			if (len >= MIN_LENGTH) {  // len >= MIN_LENGTH; we may print
				// Print line saved so far
				for (int i = 0; i < MIN_LENGTH; ++i)
					putchar(line[i]);

				// Flush the rest out until we get to the end of input or the next line
				while ((c = getchar()) != EOF) {
					putchar(c);
					if (c == '\n') {  // Move on to next line
						len = 0;
						break;
					}
				}
			}
		} else {  // New line
			if (len >= MIN_LENGTH) {  // Print the line if it's long enough
				line[len] = '\0';
				printf("%s\n", line);
			}
			len = 0;  // Move to next line
		}
	}

	return EXIT_SUCCESS;
}
