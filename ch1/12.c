#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int
main(void)
{
	int c;

	while ((c = getchar()) != EOF)
		if (isspace(c))
			putchar('\n');
		else
			putchar(c);

	return EXIT_SUCCESS;
}
