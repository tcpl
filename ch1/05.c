/*
 * Exercise 1-5:
 * Modify the temperature conversion program to print the table in reverse order, that is, from
 * 300 degrees to 0.
 *
 * Example program to modify given in file 'tempconv.c'
 */

#include <stdlib.h>
#include <stdio.h>

// Convert the numbers in STEP increments between LOWER and UPPER.
#define LOWER 0
#define UPPER 300
#define STEP  20

int
main(void)
{
	float fahr, celsius;

	for (fahr = UPPER; fahr >= LOWER; fahr -= STEP) {
		/*
		 * Basic Fahrenheit -> Celcius formula.
		 * The 'celsius = ...' step is not necessary; the '...' could be used
		 * as an argument to printf() by itself.  But, this is just a learning tool.
		 */
		celsius = (5.0/9.0) * (fahr-32);

		// In x.yf: x is the width to pad with spaces to, and y is the decimal precision
		printf("%3.0f %6.1f\n", fahr, celsius);
	}

	return EXIT_SUCCESS;
}
